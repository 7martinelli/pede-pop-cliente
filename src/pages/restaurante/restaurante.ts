import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import {AngularFirestore, AngularFirestoreCollection,
AngularFirestoreDocument} from 'angularfire2/firestore';

@IonicPage()
@Component({
  selector: 'page-restaurante',
  templateUrl: 'restaurante.html',
})
export class RestaurantePage {

  mesa: any;
  restaurante: any;

  ordensCollection: AngularFirestoreCollection<Ordem>;
  ordensObservable: Observable<Ordem[]>;

  constructor(public navCtrl: NavController,
    private afs: AngularFirestore,
    public navParams: NavParams) {
    
    this.ordensCollection = this.afs.collection('restaurantes');
    this.ordensObservable = this.ordensCollection.valueChanges();
    
  }


  gotocategorias() {
    let mesa = this.navParams.get('mesa');
    console.log(mesa);
    let restaurante = this.navParams.get('restaurante');
    console.log(restaurante);
    this.navCtrl.push('CategoriasPage', {
      restaurante: restaurante,
      mesa: mesa
    });
  }


}
