import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

declare var QRReader:any;
declare var snackbar:any;

@IonicPage()
@Component({
  selector: 'page-qr',
  templateUrl: 'qr.html',
})
export class QrPage {

    helpText:any;
    appOverlay:any;
    frame:any;
    scanningEle:any;
    iOS:any;
    copiedText:any;
    selectPhotoBtn:any;
    dialogElement:any;
    dialogOverlayElement:any;
    dialogOpenBtnElement:any;
    dialogCloseBtnElement:any;
    textBoxEle:any;
    infoSvg:any;
    videoElement:any;

    //tempo para voltar, em segundos.
    timerCount:number = 20;

    timerId:number;

    constructor(public navCtrl: NavController,
        public navParams: NavParams) {
       
        this.iOS = ['iPad', 'iPhone', 'iPod'].indexOf(navigator.platform) >= 0;    
      }



      ionViewDidLoad(){

        this.copiedText = null;
        this.frame = null;
        this.selectPhotoBtn = document.querySelector('.app__select-photos');
        this.dialogElement = document.querySelector('.app__dialog');
        this.dialogOverlayElement = document.querySelector('.app__dialog-overlay');
        this.dialogOpenBtnElement = document.querySelector('.app__dialog-open');
        this.dialogCloseBtnElement = document.querySelector('.app__dialog-close');
        this.scanningEle = document.querySelector('.custom-scanner');
        this.textBoxEle = document.querySelector('#result');
        this.helpText = document.querySelector('.app__help-text');
        this.infoSvg = document.querySelector('.app__header-icon svg');
        this.videoElement = document.querySelector('video');
        this.appOverlay = document.querySelector('.app__overlay');

        QRReader.init();

        setTimeout(() => {
            this.setCameraOverlay();
            if (!this.iOS) {
                this.scan();
              }
        }, 1000);

        //20 segundos e ele voltar para a home
        
        this.timerId = setInterval(()=>{
            if(this.timerCount ==0){
                clearInterval(this.timerId);
                this.voltar();
            }
            else{
                this.timerCount -= 1;
            }
        }, 1000);
      }

      setCameraOverlay() {
        this.appOverlay.style.borderStyle = 'solid';
        this.helpText.style.display = 'block';
      }

      createFrame() {
        this.frame = document.createElement('img');
        this.frame.src = '';
        this.frame.id = 'frame';
      }

      //sucesso no scan do qrcode
      scan() {
        this.scanningEle.style.display = 'block';
        QRReader.scan((result) => {
          /* this.copiedText = result;
          this.textBoxEle.value = result;
          this.textBoxEle.select();
          this.scanningEle.style.display = 'none'; */
         
          try{
            //parse do json no qrCode
            var jsonValue = JSON.parse(result);

            if(jsonValue && jsonValue.restaurante && jsonValue.mesa){
                this.navCtrl.setRoot('RestaurantePage', jsonValue);
            }
            else{
                snackbar.show(`Valor do qr code invalido "${result}"`)
            }

          }catch(e){
            snackbar.show(e, 5000);
          }

          //this.dialogElement.classList.remove('app__dialog--hide');
          //this.dialogOverlayElement.classList.remove('app__dialog--hide');
        });
      }

      ionViewWillLeave(){
          QRReader.webcam.srcObject.getTracks()[0].stop();
          clearInterval(this.timerId);
      }

      voltar(){
            if(this.navCtrl.canGoBack()){
                this.navCtrl.pop();
            }
            else{
                this.navCtrl.setRoot('HomePage');
            }
          
      }
}