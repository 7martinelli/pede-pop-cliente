import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

export interface Ordem {
  horário?: string;
  instrução?: string;
  item?: string;
  mesa?: string;
  status?: string;
  valor?: number;
  restaurante?: string;
}

@IonicPage()
@Component({
  selector: 'page-detalhe',
  templateUrl: 'detalhe.html',
})
export class DetalhePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  gotoconta() {
    let mesa = this.navParams.get('mesa');
    console.log(mesa);
    let restaurante = this.navParams.get('restaurante');
    console.log(restaurante);
    let categoria = this.navParams.get('categoria');
    console.log(categoria);
    let item = this.navParams.get('item');
    console.log(item);
    let valor = this.navParams.get('valor');
    console.log(valor);
    let horário = Date.now();
    console.log(horário);
    this.navCtrl.setRoot('ContaPage', {
      restaurante: restaurante,
      mesa: mesa,
      categoria: categoria,
      item: item,
      valor: valor,
      instrução: "sem cebola",
      status: "enviado",
      horário: horário
    });
  }


}
