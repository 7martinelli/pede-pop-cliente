import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController } from 'ionic-angular';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

interface User {
  celular: number;
}

@IonicPage()
@Component({
  selector: 'page-saiba',
  templateUrl: 'saiba.html',
})
export class SaibaPage {

  usersCol: AngularFirestoreCollection<User>;
  users: Observable<User[]>;


  constructor(public navCtrl: NavController,
    public afs: AngularFirestore,
    public alertCtrl: AlertController) {
    
  }

  ionViewDidLoad(){
    this.usersCol = this.afs.collection('users-promo');
    this.users = this.usersCol.valueChanges();
  }

  dism() {
    this.navCtrl.setRoot('HomePage');
  }

  telefone(users: User) {
    
      let prompt = this.alertCtrl.create({
        title: 'Promoções',
        message: "Digite seu whatsapp e receberá promoções incríveis. PS: Não vamos ficar te incomodando :)",
        inputs: [
          {
            name: 'celular',
            placeholder: 'Telefone',
            type: 'number'
          },
        ],
        buttons: [
          {
            text: 'Cancelar',
            handler: data => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Salvar',
            handler: data => {
              console.log('Saved clicked');
            }
          }
        ]
      });
      prompt.present();
    }
  
}
