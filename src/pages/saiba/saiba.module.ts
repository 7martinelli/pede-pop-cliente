import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SaibaPage } from './saiba';

@NgModule({
  declarations: [
    SaibaPage,
  ],
  imports: [
    IonicPageModule.forChild(SaibaPage),
  ],
})
export class SaibaPageModule {}
