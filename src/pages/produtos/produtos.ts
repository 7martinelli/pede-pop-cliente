import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-produtos',
  templateUrl: 'produtos.html',
})
export class ProdutosPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }


  gotodetalhe() {
    let mesa = this.navParams.get('mesa');
    console.log(mesa);
    let restaurante = this.navParams.get('restaurante');
    console.log(restaurante);
    let categoria = this.navParams.get('categoria');
    console.log(categoria);
    this.navCtrl.push('DetalhePage', {
      restaurante: restaurante,
      mesa: mesa,
      categoria: categoria,
      item: "Filet a parmegiana",
      valor: "3.90"
    });
  
}

}
