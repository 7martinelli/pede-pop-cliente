import { Component } from '@angular/core';
import { IonicPage , NavController, ModalController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController,
    public modalCtrl: ModalController) {

  }

  scancode() {
    // * this.navCtrl.setRoot('QrPage'); - uncomment for production*/
    this.navCtrl.setRoot('RestaurantePage', {
      restaurante: "Agnus",
      mesa: "08"
    });
  }

  gotosaiba() {
    let modal = this.modalCtrl.create('SaibaPage');
    modal.present();
  }

}