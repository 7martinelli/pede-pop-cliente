import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-categorias',
  templateUrl: 'categorias.html',
})
export class CategoriasPage {


  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {

    let mesa = this.navParams.get('mesa');
    console.log(mesa);
    let restaurante = this.navParams.get('restaurante');
    console.log(restaurante);
    let categoria = this.navParams.get('categoria');
    console.log(categoria);


  }

  gotoprodutos() {
    let mesa = this.navParams.get('mesa');
    console.log(mesa);
    let restaurante = this.navParams.get('restaurante');
    console.log(restaurante);
    this.navCtrl.push('ProdutosPage', {
      restaurante: restaurante,
      mesa: mesa,
      categoria: "bebidas"
    });
  }

}
