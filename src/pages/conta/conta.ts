import { HomePage } from './../home/home';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController } from 'ionic-angular';

export interface Ordem {
  horário?: string;
  instrução?: string;
  item?: string;
  mesa?: string;
  status?: string;
  valor?: number;
  restaurante?: string;
}


@IonicPage()
@Component({
  selector: 'page-conta',
  templateUrl: 'conta.html',
})
export class ContaPage {
  testRadioResult: any;
  testRadioOpen: boolean;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    let mesa = this.navParams.get('mesa');
    console.log(mesa);
    let restaurante = this.navParams.get('restaurante');
    console.log(restaurante);
    let categoria = this.navParams.get('categoria');
    console.log(categoria);
    let item = this.navParams.get('item');
    console.log(item);
    let valor = this.navParams.get('valor');
    console.log(valor);
    let instrução = this.navParams.get('instrução');
    console.log(instrução);
    let status = this.navParams.get('status');
    console.log(status);
    let horário = this.navParams.get('horário');
    console.log(horário);
   
  }


  fechar() {
    let confirm = this.alertCtrl.create({
      title: 'Fechar Conta',
      message: 'Você deseja fechar a sua conta?',
      buttons: [
        {
          text: 'Não',
          handler: () => {
            console.log('não');
          }
        },
        {
          text: 'Sim',
          handler: () => {
            console.log('sim');
            this.presentToast();
            this.navCtrl.setRoot(HomePage);

          }
        }
      ]
    });
    confirm.present();
  }

  gotocategorias() {
    this.navCtrl.push('CategoriasPage');
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Obrigado por usar o PEDE POP',
      duration: 2000
    });
    toast.present();
  }


}

 
